package com.bionexo.calculator.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bionexo.calculator.dto.UbsResponseDto;
import com.bionexo.calculator.util.Util;

@Service
public class DistanceCalculatorService {

	private static double LIMIT = 1;

	public UbsResponseDto obtainCsv(String query, String page, String perPage) throws IOException {

		List<UbsResponseDto> ubsDtos = new ArrayList<>();

		String fname = "C:\\Users\\x216564\\Downloads\\ubs.csv";

		try (BufferedReader sr = new BufferedReader(new InputStreamReader(new FileInputStream(fname)))) {

			String line;
			sr.readLine(); // pula uma linha
			String[] queryCols = query.split(",");
			Double lat = Double.valueOf(queryCols[0]);
			Double lng = Double.valueOf(queryCols[1]);
			while ((line = sr.readLine()) != null) {
				String[] cols = line.split(",");
				UbsResponseDto ubsDto = new UbsResponseDto();
				Double csvLat = Double.valueOf(cols[0]);
				Double csvLng = Double.valueOf(cols[1]);

				if (Math.abs(csvLat - lat) < LIMIT && Math.abs(csvLng - lng) < LIMIT) {

					double dist = Util.distanceCoord(csvLat, csvLng, lat, lng, "K");
					ubsDto.setDistance(dist);
					ubsDto.getGeocode().setLat(lat);
					ubsDto.getGeocode().setLonge(lng);
					ubsDto.setId(Integer.parseInt(cols[2]));
					ubsDto.setName(cols[4]);
					ubsDto.setAddress(cols[5]);
					ubsDto.setCity(cols[7]);
					ubsDto.setPhone(cols[8]);
					ubsDto.getScores().setAdaptationForSeniors(cols[10]);
					ubsDto.getScores().setMedicalEquipment(cols[11]);
					ubsDto.getScores().setMedicine(cols[12]);
					ubsDtos.add(ubsDto);
				}
			}
			Collections.sort(ubsDtos);
		}
		return ubsDtos.isEmpty() ? new UbsResponseDto() : ubsDtos.get(0);
	}

}
