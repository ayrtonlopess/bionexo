package com.bionexo.calculator.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UbsResponseDto implements Comparable<UbsResponseDto> {

	private long id;
	private String name;
	private String address;
	private String city;
	private String phone;
	private GeocodeDto geocode;
	private ScoresDto scores;
	@JsonIgnore
	private double distance;

	public UbsResponseDto() {

	}

	public GeocodeDto getGeocode() {
		if (geocode == null) {
			geocode = new GeocodeDto();
		}
		return geocode;
	}

	public void setGeocode(GeocodeDto geocode) {
		if (geocode == null) {
			geocode = new GeocodeDto();
		}
		this.geocode = geocode;
	}

	public ScoresDto getScores() {
		if (scores == null) {
			scores = new ScoresDto();
		}
		return scores;
	}

	public void setScores(ScoresDto scores) {
		if (scores == null) {
			scores = new ScoresDto();
		}
		this.scores = scores;
	}

	
	@Override
	public int compareTo(UbsResponseDto o) {
		if (o == null) throw new NullPointerException("Cannot compare UbsResponseDto to null");
		return (int) Math.signum(this.distance - o.distance);
	}


}
