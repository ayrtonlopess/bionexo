package com.bionexo.calculator.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class ScoresDtoTest {
	
	@Test
	public void testScores() {
		ScoresDto score = new ScoresDto();
		
		score.setAdaptationForSeniors("Desempenho muito acima da média");
		score.setMedicalEquipment("Desempenho acima da média");
		score.setMedicine("Desempenho muito acima da média");
		score.setSize(3);
		
		assertEquals("Desempenho muito acima da média" , score.getAdaptationForSeniors());
		assertEquals("Desempenho acima da média", score.getMedicalEquipment());
		assertEquals("Desempenho muito acima da média", score.getMedicine());
		assertEquals(3, score.getSize());
		
	}

}
