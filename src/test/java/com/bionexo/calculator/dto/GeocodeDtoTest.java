package com.bionexo.calculator.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class GeocodeDtoTest {
	
	
	@Test
	public void testGeocode () {
		GeocodeDto geoCode = new GeocodeDto();
		
		geoCode.setLat(-4565777D);
		geoCode.setLonge(-2343545D);
		
		assertEquals(-4565777 , geoCode.getLat());
		assertEquals(-2343545 , geoCode.getLonge());
	}
}
