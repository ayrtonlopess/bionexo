package com.bionexo.calculator.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class UbsResponseDtoTest {

	@Test
	public void testUbs() {
		UbsResponseDto ubs = new UbsResponseDto();

		ubs.setAddress("RUA ANTONIO JOAQUIM RODRIGUES");
		ubs.setCity("Nova Santa Bárbara");
		ubs.setDistance(3);
		ubs.setId(1);
		ubs.setName("CENTRO DE SAUDE NOVA STA BARBARA");
		ubs.setPhone("(43)2661253");

		assertEquals("RUA ANTONIO JOAQUIM RODRIGUES", ubs.getAddress());
		assertEquals("Nova Santa Bárbara", ubs.getCity());
		assertEquals(3, ubs.getDistance());
		assertEquals(1, ubs.getId());
		assertEquals("CENTRO DE SAUDE NOVA STA BARBARA", ubs.getName());
		assertEquals("(43)2661253", ubs.getPhone());

	}

}
