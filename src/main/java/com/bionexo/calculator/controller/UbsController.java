package com.bionexo.calculator.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bionexo.calculator.dto.UbsResponseDto;
import com.bionexo.calculator.service.DistanceCalculatorService;

@RestController
@RequestMapping("/api/v1")
public class UbsController {

	@Autowired
	private DistanceCalculatorService service;

	@GetMapping("/find_ubs")
	public ResponseEntity<UbsResponseDto> findUbs(@RequestParam("query") final String query,
			@RequestParam("page") final String page, @RequestParam("per_page") final String perPage)
			throws IOException {
		return ResponseEntity.ok(this.service.obtainCsv(query, page, perPage));

	}

}
