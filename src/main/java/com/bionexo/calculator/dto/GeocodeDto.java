package com.bionexo.calculator.dto;

import lombok.Data;

@Data
public class GeocodeDto {
	
	private Double lat;
	private Double longe;
}
