package com.bionexo.calculator.dto;

import lombok.Data;

@Data
public class ScoresDto {
	
	private long size;
	private String adaptationForSeniors;
	private String medicalEquipment;
	private String medicine;
}
