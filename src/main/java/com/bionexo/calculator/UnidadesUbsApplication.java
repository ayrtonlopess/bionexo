package com.bionexo.calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnidadesUbsApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnidadesUbsApplication.class, args);
	}

}
